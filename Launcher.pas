unit Launcher;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Registry, ShellApi, IdGlobal, Psapi, tlhelp32;

type
  TDialogEx = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    ComboBox1: TComboBox;
    ComboBox2: TComboBox;
    Label1: TLabel;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    Edit1: TEdit;
    Button2: TButton;
    Button3: TButton;
    PaintBox1: TPaintBox;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    RadioButton6: TRadioButton;
    Label5: TLabel;
    RadioButton8: TRadioButton;
    FScreen: TCheckBox;
    NoVid: TCheckBox;
    Mcam: TCheckBox;
    ScreenS: TCheckBox;
    Aport: TCheckBox;
    NoSound: TCheckBox;
    NoMusic: TCheckBox;
    Button4: TButton;
    ComboBox3: TComboBox;
    Label6: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure ComboBox1Select(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure PaintBox1Paint(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
 {   procedure SetMtype(m_type:integer);
    function GetMtype:integer; }
    function TestFiles(Fileone, Filetwo:string):boolean;
  end;

var
  DialogEx: TDialogEx;

implementation

{$R *.dfm}

type 
  TRefresh = record
    WRes: Integer;
    HRes: Integer;         
    Fre: array of Integer;
  end;

var
  TableRefresh: array of TRefresh;
  defWRes, defHRes, defFre: Integer;
  mtype, Atype, wtype: Integer;
  UserFile, Dir, PackFile: string;
  Bmp: TBitmap;
  n: Cardinal;
  si: STARTUPINFO;
  Pi: PROCESS_INFORMATION;
  Aspect: Boolean;
  XAspect, YAspect, tempXAsp, tempYAsp: Single;
  x_off, y_off, hackBuf, adrhack: Pointer;


function NotFindRef(WRes, HRes: Integer; var outidx: Integer): Boolean;
var
  Len, j: Integer;
begin
  Result := false;
  Len := Length(TableRefresh);
  j := 0;
  while j < Len do 
  begin
    outidx := j;
    if (TableRefresh[j].HRes = HRes) and (TableRefresh[j].WRes = WRes) then 
      Exit;
    Inc(j);
  end;
  Result := true;
end;
{
procedure TDialogEx.SetMtype(m_type:integer);
begin
case m_type of
0:RadioButton1.Checked :=true;
1:RadioButton4.Checked :=true;
2:RadioButton5.Checked :=true;
end;
end; 

function TDialogEx.GetMtype:integer;
begin
if RadioButton1.Checked then result:=0
else if RadioButton4.Checked then result:=1
else if RadioButton5.Checked then result:=2;
end;   }

procedure TDialogEx.FormCreate(Sender: TObject);
var
  i,k,idx:integer;
  DC: THandle;      // display context
  Bits: integer;    // bits per pixel
  HRes: integer;    // horizontal resolution
  VRes: integer;    // vertical resolution
  DM: TDevMode;     // to Save EnumDisplaySettings
  ModeNum: longint; // video mode number
  Ok: Bool;
  fre: integer;     // refresh rate
    sr: TSearchRec;
  dName: string;
reg:TRegistry;
        function ReadRegInt(RegStr:string;def:integer):integer;
        begin
        if reg.ValueExists(RegStr) then Result:=reg.ReadInteger(RegStr)else
        Result:=def;
        end;
        function ReadRegBool(RegStr:string;def:boolean):boolean;
        begin
        if reg.ValueExists(RegStr) then Result:=reg.ReadBool(RegStr)else
        Result:=def;
        end;
begin
reg := TRegistry.Create;
reg.RootKey:=HKEY_LOCAL_MACHINE;
reg.OpenKey('Software\AlexBond\W4Launcher',true);
Dir:=GetCurrentDir;
//UserFile:=reg.ReadString('UserFile');
  DC   := GetDC(Handle); 
  Bits := GetDeviceCaps(DC, BITSPIXEL);
  HRes := GetDeviceCaps(DC, HORZRES);
  VRes := GetDeviceCaps(DC, VERTRES); 
  fre  := GetDeviceCaps(DC, VREFRESH);

  ReleaseDC(Handle, DC); // Show all modes available
  ModeNum := 0;  // The 1st one

  defWRes:=ReadRegInt('WRes',HRes);
  defHRes:=ReadRegInt('HRes',VRes);
  defFre:=ReadRegInt('Fre',fre);
//  mtype:=ReadRegInt('SelectNumber',0);
  Atype:=ReadRegInt('Aspect',0);
  PackFile:=reg.ReadString('PackFile');
 // W3dtOn.Checked:=ReadRegBool('W3DTexture', True);
 // W3dtOff.Checked:= Not W3dtOn.Checked;

  FScreen.Checked:=ReadRegBool('FScreen', False);
  NoVid.Checked:=ReadRegBool('NoVid', False);
  NoSound.Checked:=ReadRegBool('NoSound', False);
  Aport.Checked:=ReadRegBool('Aport', False);
  ScreenS.Checked:=ReadRegBool('ScreenS', False);
  Mcam.Checked:=ReadRegBool('Mcam', False);
  NoMusic.Checked:=ReadRegBool('NoMusic' ,False);

case Atype of
0: RadioButton6.Checked:=true;
1: RadioButton8.Checked:=true;
end;

Edit1.Text:=reg.ReadString('Cmdline');
//SetMtype(mtype);
reg.CloseKey;

  EnumDisplaySettings(nil, ModeNum, DM);
  ComboBox1.Items.Clear;

  Ok := True;
  while Ok do
  begin 
    Inc(ModeNum); // Get next one
    Ok := EnumDisplaySettings(nil, ModeNum, DM);
    if (DM.dmBitsPerPel=Bits) then begin
    if NotFindRef(DM.dmPelsWidth, DM.dmPelsHeight,i) then
        begin
        i:=Length(TableRefresh);
        SetLength(TableRefresh,i+1);
        TableRefresh[i].HRes:=DM.dmPelsHeight;
        TableRefresh[i].WRes:=DM.dmPelsWidth;
        ComboBox1.Items.Add(Format('%dx%d', [DM.dmPelsWidth, DM.dmPelsHeight]));
        if  (DM.dmPelsWidth=defWRes) and (DM.dmPelsHeight=defHRes) then
        idx:=i;
        end;
    k:=Length(TableRefresh[i].Fre);
    SetLength(TableRefresh[i].Fre,k+1);
    TableRefresh[i].Fre[k]:=DM.dmDisplayFrequency;
      end;
  end;
  ComboBox1.ItemIndex:=idx;
  ComboBox1Select(sender);
  // make MapPacks list
  idx:=-1;
   if SysUtils.FindFirst('Data\MapPacks\Scripts_*.xml', faAnyFile, sr) = 0 then
  begin
    repeat
      if (sr.attr and faDirectory) <> faDirectory  then
      begin
        dName:=Copy(sr.Name,9,Pos('.xml',sr.Name)-9);
        i:=ComboBox3.Items.Add(dName);
        if dName='Standard' then k:=i;
        if PackFile=dName then idx:=i;
      end;
    until
      SysUtils.FindNext(sr) <> 0;
  end;
  SysUtils.FindClose(sr);
  if idx=-1 then idx:=k;
  ComboBox3.ItemIndex:=idx;

  Bmp:=TBitmap.Create;
  Bmp.LoadFromResourceID(hInstance,131);
end;

procedure TDialogEx.ComboBox1Select(Sender: TObject);
var
len,j,i,idx:integer;
begin
i:=ComboBox1.ItemIndex;
ComboBox2.Items.Clear;
len:=Length(TableRefresh[i].Fre);
idx:=0;
for j:=0 to Len-1 do begin
 ComboBox2.Items.Add(Format('%dHz', [TableRefresh[i].Fre[j]]));
 if TableRefresh[i].Fre[j]=defFre then idx:=j;
 end;
ComboBox2.ItemIndex:=idx;
end;

procedure TDialogEx.Button2Click(Sender: TObject);
begin
Close;
end;

procedure TDialogEx.PaintBox1Paint(Sender: TObject);
begin
PaintBox1.Canvas.Draw(0,0,Bmp);
end;

procedure TDialogEx.Button1Click(Sender: TObject);
var
reg:TRegistry;
begin
{reg := TRegistry.Create;
reg.RootKey:=HKEY_LOCAL_MACHINE;
reg.OpenKey('Software\AlexBond\W4Launcher',true);
OpenDialog1.FileName:=UserFile;
  if OpenDialog1.Execute and (OpenDialog1.FileName<>'') then
  begin
  UserFile:=OpenDialog1.FileName;
  reg.WriteString('UserFile',UserFile);
  end;         
reg.CloseKey;    }
end;

function TDialogEx.TestFiles(Fileone, Filetwo:string):boolean;
begin
result:=false;
if not FileExists(Filetwo) then begin MessageBox(Handle,Pchar(format('%s not found!!!',[Filetwo])), 'Error', MB_OK); exit end;
if not FileExists(Fileone) then begin result:=true; exit; end;
if (FileSizeByName(Fileone)=FileSizeByName(Filetwo)) then exit;
result:=true;
end;


function GetModuleAdress(ProcessID:DWORD;ModuleName:String):Pointer;
var
  hSnapShot: THandle;
   ModuleInfo:TModuleEntry32;
begin
Result:=0;
  hSnapShot := CreateToolHelp32Snapshot(TH32CS_SNAPMODULE, ProcessID);
  if (hSnapShot <> THandle(-1)) then
  begin
    ModuleInfo.dwSize := SizeOf(ModuleInfo);
    if (Module32First(hSnapshot, ModuleInfo)) then
    begin
      while (Module32Next(hSnapShot, ModuleInfo)) do
        if (ModuleInfo.szModule=ModuleName) then
        begin
        Result :=ModuleInfo.modBaseAddr;
        CloseHandle(hSnapShot);
        exit; end;
    end;
    CloseHandle(hSnapShot);
  end;
end;

var
 PNewAdress,BaseAdress:pointer;

var asmcode: array[0..25] of byte = (
($8b),($44),($24),($04),                //mov eax,[esp+4]
($d9),($40),($14),                      //fld [eax+14]
($c7),($00),($cd),($cc),($cc),($3f),    //mov [eax],$3fcccccd
($d8),($30),                            //fdiv [eax]
($d9),($18),                            //fstp [eax]
($50),                                  //push eax
($ff),($15),($18),($64),($77),($00),    //call dword ptr [$00776418]
($c2),($04));                           //ret $0004
{
type
 TMatrix = array[1..4, 1..4] of Single;
 PMatrix = ^TMatrix;
//procedure glLoadMatrixf(m: PGLfloat); stdcall;
 PFunction = procedure (const m:PMatrix);stdcall;   

procedure AspectMatrix(const m:pointer);stdcall;
begin

asm
mov eax,m
fld [eax+20]
mov [eax],$3FCCCCCD
fdiv [eax]
fstp [eax]
push eax
call dword ptr [$00776418]
end;

end;
procedure AspectMatrixEnd; begin end;           }

procedure DeleteDir(const Path: string);
var 
  Found: Integer;
  SearchRec: TSearchRec;
  FileName: string;
begin
  Found := FindFirst(Path + '/*.*', faAnyFile, SearchRec);
  while Found = 0 do
  begin
    if ((SearchRec.Attr and faDirectory) = faDirectory) then
      if (SearchRec.Name <> '.') and (SearchRec.Name <> '..') then 
        DeleteDir(Path + '/' + SearchRec.Name)
      else
    else
    begin
      FileName := Path + '/' + SearchRec.Name + #0;
      DeleteFile(PChar(FileName));
    end;
    Found := FindNext(SearchRec);
  end;
  SysUtils.FindClose(SearchRec);
  RemoveDir(Path);
end;

procedure TDialogEx.Button3Click(Sender: TObject);
var
  hProcess: THandle;
  pLL, pDLLPath: Pointer;
  LibPathLen, _WR, ThrID,SizeFunc: DWORD;
  Ok: Boolean;
  LibName: PChar;
  FileName, FileName2, ScriptFile, WormFile, dName, Cmd_line: string;
  reg: TRegistry;
  i, j: Integer;
  FOut:TextFile;
begin
  reg := TRegistry.Create;
  reg.RootKey := HKEY_LOCAL_MACHINE;
  reg.OpenKey('Software\AlexBond\W4Launcher', true);
//  mtype := GetMtype;
  FileName := Dir + '/Data/Temp/Scripts.xml';
  dName:=ComboBox3.Text;
  ScriptFile := Dir + '/Data/MapPacks/Scripts_'+dName+'.xml';
  SetCurrentDir(Dir);
  if not DirectoryExists('Data/Temp/') then CreateDir('Data/Temp/');

if not FileExists(ScriptFile) then MessageBox(Handle,Pchar(format('%s not found!!!',[ScriptFile])), 'Error', MB_OK)
else
  CopyFile(Pchar(ScriptFile),Pchar(FileName),false);

  i := ComboBox1.ItemIndex;
  j := ComboBox2.ItemIndex;
  Atype := Integer(RadioButton8.Checked);

  // ��������� ���� Launcher.cfg
    AssignFile(FOut, 'launcher.cfg');
    Rewrite(FOut);
    Write(FOut,Format('/W:%d /H:%d /REFRESH:%d',[TableRefresh[i].WRes,TableRefresh[i].HRes,TableRefresh[i].Fre[j]]));
	if(FScreen.Checked)  then Write(FOut,' /FS');
	if(NoVid.Checked)  then Write(FOut,' /NOMOVIES');
	if(NoSound.Checked)  then Write(FOut,' /NOSOUND');
	if(Aport.Checked)  then Write(FOut,' /ENABLEPORTFORWARDING');
	if(ScreenS.Checked)  then Write(FOut,' /ALLOWSCREENSHOTS');
	if(Mcam.Checked)  then Write(FOut,' /MANUALCAMERA');
	if(NoMusic.Checked)  then Write(FOut,' /NOMUSIC');

    CloseFile(FOut);
  reg.WriteString('PackFile',dName);
  reg.WriteBool('FScreen', FScreen.Checked);
  reg.WriteBool('NoVid', NoVid.Checked);
  reg.WriteBool('NoSound', NoSound.Checked);
  reg.WriteBool('Aport', Aport.Checked);
  reg.WriteBool('ScreenS', ScreenS.Checked);
  reg.WriteBool('Mcam', Mcam.Checked);
  reg.WriteBool('NoMusic', NoMusic.Checked);

 // reg.WriteBool('W3DTexture', W3dtOn.Checked);

//  reg.WriteInteger('SelectNumber', mtype);
  reg.WriteString('Cmdline', Edit1.Text);
  reg.WriteInteger('WRes', TableRefresh[i].WRes);
  reg.WriteInteger('HRes', TableRefresh[i].HRes);
  reg.WriteInteger('Fre', TableRefresh[i].Fre[j]);
  reg.WriteInteger('Aspect', Atype);
  reg.CloseKey;

  Cmd_line := Format(' %s', [Edit1.Text]);

  Aspect := Atype > 0;
  if Aspect then
  begin
    zeromemory(@si, SizeOf(si));
    if (not CreateProcess(nil, PChar(Dir + '\WORMS 4 MAYHEM.EXE' + Cmd_line),
      nil, nil, false, 0, nil, nil, si, Pi)) then
      MessageBox(Handle, 'WORMS 4 MAYHEM.EXE not found!!!', 'Error', MB_OK)
    else 
    begin
      if Aspect then 
      begin
        N := 0;
        DialogEx.Hide;
        Sleep(10000);
        tempXAsp:=TableRefresh[i].WRes/TableRefresh[i].HRes; // 16:10
        x_off := Pointer(0);
        ReadProcessMemory(Pi.hProcess, x_off, @XAspect, SizeOf(XAspect), N);
        //    LoadMatrix(hackBuf);
        SizeFunc:= sizeof(AsmCode);//dword(@AspectMatrixEnd) - dword(@AspectMatrix);
        hackBuf := VirtualAllocEx(Pi.hProcess, 0, SizeFunc + 16,
          MEM_COMMIT, PAGE_READWRITE);
        Single(pointer(@AsmCode[9])^):=tempXAsp;
        WriteProcessMemory(Pi.hProcess, hackBuf, @AsmCode, SizeFunc, N);
        adrhack := Pointer(Cardinal(hackBuf) + SizeFunc);
        WriteProcessMemory(Pi.hProcess, adrhack, @hackBuf, SizeOf(hackBuf), N);
        WriteProcessMemory(Pi.hProcess, Pointer($00461BBC),  //0c FF 15 18 +3bit
          @adrhack, SizeOf(adrhack), N);
      end;
    {
      if W3dtOn.Checked then
      begin

        //  LoadLibrary;
        LibName := 'w3dmapper.dll';
        if not FileExists(LibName) then begin MessageBox(Handle,Pchar(format('%s not found!!!',[LibName])), 'Error', MB_OK); close; exit; end;

        LibPathLen := Length(string(LibName));

        pDLLPath := VirtualAllocEx(Pi.hProcess, 0, LibPathLen + 1,
          MEM_COMMIT, PAGE_READWRITE);
        pLL := GetProcAddress(GetModuleHandle(kernel32), 'LoadLibraryA');
        WriteProcessMemory(Pi.hProcess, pDLLPath, LibName, LibPathLen + 1, _WR);
        CreateRemoteThread(Pi.hProcess, 0, 0, pLL, pDLLPath, 0, ThrID);

        Sleep(5000);
        BaseAdress := GetModuleAdress(Pi.dwProcessId, 'w3dmapper.dll');
        if BaseAdress <> nil then
        begin
          //RewriteFunction
          PNewAdress := Pointer(Cardinal(BaseAdress) + Cardinal($196f4));
          pDLLPath := Pointer($007761bc); // 08 FF 15 BC 61
          WriteProcessMemory(Pi.hProcess, pDLLPath,
            @PNewAdress, SizeOf(Pointer), _WR); //007761bc
          Sleep(1000);
        end;
      end
      else
      begin
      DeleteDir('Data/Temp/Custom/');
      DeleteDir('Data/Temp/ThemeCamelot/');
      end;  }
      Close;
      Exit;
    end;
  end
  else 
  begin
    if WinExec(PChar(Dir + '\WORMS 4 MAYHEM.EXE' + Cmd_line),
      SW_SHOWNORMAL) = ERROR_FILE_NOT_FOUND then
      MessageBox(Handle, 'WORMS 4 MAYHEM.EXE not found!!!',
        'Error', MB_OK)
    else
    Close;
  end;
end;


procedure TDialogEx.Button4Click(Sender: TObject);
begin
  ShellExecute(0, nil, 'http://www.wormsmayhem.com/support.html?subsection=patch&CRC=0x125FFDA8',
    nil, nil, 1);
end;

end.
