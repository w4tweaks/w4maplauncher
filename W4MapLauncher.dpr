program W4MapLauncher;

uses
  Forms,
  Launcher in 'Launcher.pas' {DialogEx};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'W4MapLauncher';
  Application.CreateForm(TDialogEx, DialogEx);
  Application.Run;
end.
