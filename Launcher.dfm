object DialogEx: TDialogEx
  Left = 698
  Top = 192
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Worms 4 Maps Launcher'
  ClientHeight = 399
  ClientWidth = 268
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 7
    Top = 6
    Width = 254
    Height = 131
    BevelInner = bvLowered
    BevelOuter = bvLowered
    Color = clGray
    TabOrder = 0
    object PaintBox1: TPaintBox
      Left = 3
      Top = 3
      Width = 248
      Height = 125
      Color = clGray
      ParentColor = False
      OnPaint = PaintBox1Paint
    end
    object Label2: TLabel
      Left = 78
      Top = 113
      Width = 106
      Height = 13
      Caption = 'W3DMaps v.2.085'
      Color = clBlack
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = True
    end
    object Label6: TLabel
      Left = 77
      Top = 114
      Width = 106
      Height = 13
      Caption = 'W3DMaps v.2.085'
      Color = clBlack
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = True
    end
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 144
    Width = 252
    Height = 65
    Caption = 'Resolution'
    TabOrder = 1
    object Label1: TLabel
      Left = 159
      Top = 20
      Width = 11
      Height = 13
      Caption = '@'
    end
    object Label5: TLabel
      Left = 11
      Top = 42
      Width = 36
      Height = 13
      Caption = 'Aspect:'
    end
    object ComboBox1: TComboBox
      Left = 8
      Top = 16
      Width = 151
      Height = 22
      Style = csOwnerDrawVariable
      BiDiMode = bdLeftToRight
      DropDownCount = 6
      ItemHeight = 16
      ParentBiDiMode = False
      TabOrder = 0
      OnSelect = ComboBox1Select
    end
    object ComboBox2: TComboBox
      Left = 173
      Top = 16
      Width = 68
      Height = 22
      Style = csOwnerDrawVariable
      DropDownCount = 6
      ItemHeight = 16
      TabOrder = 1
    end
    object FScreen: TCheckBox
      Left = 173
      Top = 40
      Width = 68
      Height = 17
      Caption = 'Fullscreen'
      TabOrder = 2
    end
    object RadioButton6: TRadioButton
      Left = 57
      Top = 41
      Width = 41
      Height = 17
      Caption = 'Off'
      Checked = True
      TabOrder = 3
      TabStop = True
    end
    object RadioButton8: TRadioButton
      Left = 105
      Top = 41
      Width = 56
      Height = 17
      Caption = 'On'
      TabOrder = 4
    end
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 213
    Width = 252
    Height = 49
    Caption = 'Maps Pack'
    TabOrder = 2
    object ComboBox3: TComboBox
      Left = 8
      Top = 16
      Width = 233
      Height = 21
      ItemHeight = 13
      TabOrder = 0
    end
  end
  object GroupBox3: TGroupBox
    Left = 8
    Top = 265
    Width = 251
    Height = 96
    Caption = 'Advanced'
    TabOrder = 3
    object Label3: TLabel
      Left = 180
      Top = 60
      Width = 58
      Height = 13
      Caption = 'AlexBond '
      Color = clBlack
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindow
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = True
    end
    object Label4: TLabel
      Left = 184
      Top = 76
      Width = 43
      Height = 13
      Caption = #169' 2010'
      Color = clBlack
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindow
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = True
    end
    object NoVid: TCheckBox
      Left = 8
      Top = 40
      Width = 81
      Height = 17
      Caption = 'No Video'
      TabOrder = 0
    end
    object Mcam: TCheckBox
      Left = 88
      Top = 40
      Width = 113
      Height = 17
      Caption = 'Manual camera (M)'
      TabOrder = 1
    end
    object ScreenS: TCheckBox
      Left = 88
      Top = 56
      Width = 81
      Height = 17
      Caption = 'Screenshots'
      TabOrder = 2
    end
    object Aport: TCheckBox
      Left = 88
      Top = 72
      Width = 82
      Height = 17
      Caption = 'Auto PORTs'
      TabOrder = 3
    end
    object NoSound: TCheckBox
      Left = 8
      Top = 56
      Width = 77
      Height = 17
      Caption = 'No Sounds'
      TabOrder = 4
    end
    object NoMusic: TCheckBox
      Left = 8
      Top = 72
      Width = 76
      Height = 17
      Caption = 'No Music'
      TabOrder = 5
    end
    object Edit1: TEdit
      Left = 8
      Top = 17
      Width = 233
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 6
    end
  end
  object Button2: TButton
    Left = 8
    Top = 368
    Width = 65
    Height = 25
    Caption = 'Cancel'
    TabOrder = 4
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 192
    Top = 368
    Width = 68
    Height = 25
    Caption = 'Play'
    TabOrder = 5
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 80
    Top = 368
    Width = 105
    Height = 25
    Caption = 'Check for Updates'
    TabOrder = 6
    OnClick = Button4Click
  end
end
